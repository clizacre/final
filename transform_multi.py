from PIL import Image, ImageFilter
import sys

def rotate_colors(image, angle):
    return image.rotate(angle)

def mirror(image):
    return image.transpose(Image.FLIP_LEFT_RIGHT)

def blur(image):
    return image.filter(ImageFilter.BLUR)

def change_colors(image, r, g, b):
    data = image.getdata()
    new_data = [(r, g, b) if pixel[0] > 128 else pixel for pixel in data]
    return Image.new("RGB", image.size).putdata(new_data)

def shift(image, dx, dy):
    return image.transform(image.size, Image.AFFINE, (1, 0, dx, 0, 1, dy))

def apply_transformations(image, transformations):
    for i in range(0, len(transformations), 2):
        transform_name = transformations[i]
        transform_args = transformations[i + 1:]
        if transform_name == "rotate_colors":
            image = rotate_colors(image, *map(int, transform_args))
        elif transform_name == "mirror":
            image = mirror(image)
        elif transform_name == "blur":
            image = blur(image)
        elif transform_name == "change_colors":
            image = change_colors(image, *map(int, transform_args))
        elif transform_name == "shift":
            image = shift(image, *map(int, transform_args))
    return image

def main():

    input_image_path = sys.argv[1]
    transformations = sys.argv[2:]

    try:
        original_image = Image.open(input_image_path)
    except IOError:
        print(f"No se puede abrir la imagen {input_image_path}")
        sys.exit(1)

    transformed_image = apply_transformations(original_image, transformations)

    output_image_path = f"transformed_{input_image_path}"
    transformed_image.save(output_image_path)
    print(f"Imagen transformada guardada en {output_image_path}")

if __name__ == "__main__":
    main()
''' if len(sys.argv) < 3 or (len(sys.argv) - 2) % 2 != 0:
        print("Uso: python3 transform_multi.py <imagen> <transformacion1> <arg1> <arg2> ... <transformacion2> <arg1> <arg2> ...")
        sys.exit(1)'''
