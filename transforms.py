def change_colors(image: list[list[tuple[int, int, int]]],
                  to_change: tuple[int, int, int],
                  to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
    for column in image:
        for pixel in range(len(column)):
            if column[pixel] == to_change:
                column[pixel] = to_change_to
    return image


def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    rotated_image = []
    for row in zip (*image):
        rotated_image.append(list(reversed(row)))
    return rotated_image

def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    image_mirror = []
    for pixel in image[::-1]:
        image_mirror.append(pixel)
    return image_mirror


def roate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    image_rotated = []
    for column in image:
        new_column = []
        for pixel in column:
            new_r = (pixel[0] + increment) % 256
            new_g = (pixel[1] + increment) % 256
            new_b = (pixel[2] + increment) % 256
            new_column.append((new_r, new_g, new_b))
        image_rotated.append(new_column)
    return image_rotated

def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    image_grayscale = []
    for column in image:
        new_column = []
        for pixel in column:
            media = int(sum(pixel)/3)
            new_column.append((media, media, media))
        image_grayscale.append(new_column)
    return image_grayscale

def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    return [[(min(int(r * pixel[0]), 255), min(int(g * pixel[1]), 255), min(int(b * pixel[2]), 255)) for pixel in row]
            for row in image]
def shift(image: list[list[tuple[int, int, int]]], rows: int, cols: int) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])

    for i in range(rows):
        for j in range(cols):
            new_i = (i + rows) % rows
            new_j = (j + cols) % cols
            image[new_i][new_j] = image[i][j]
    return image

def crop(image: list[list[tuple[int, int, int]]], top: int, left: int, bottom: int, right: int) -> list[list[tuple[int, int, int]]]:
    image_crop = []
    for row in range(left, left + right):
        new_row = []
        for col in range(top, top + bottom):
            new_row.append(image[row][col])
        image_crop.append(new_row)
    return image_crop

def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    rows, cols = len(image), len(image[0])

    result = [[(0, 0, 0) for _ in range(cols)] for _ in range(rows)]

    for i in range(1, rows - 1):

        for j in range(1, cols - 1):
            r = (image[i - 1][j][0] + image[i + 1][j][0] + image[i][j - 1][0] + image[i][j + 1][0]) // 4

            g = (image[i - 1][j][1] + image[i + 1][j][1] + image[i][j - 1][1] + image[i][j + 1][1]) // 4

            b = (image[i - 1][j][2] + image[i + 1][j][2] + image[i][j - 1][2] + image[i][j + 1][2]) // 4

            result[i][j] = (r, g, b)

    return result

